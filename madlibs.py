import random

roles=["knight","queen","general","wizard","dragon", "gamer", "student"]
names=["Luke","Leia","Han","Harry","Hermione","Ron","Katniss", "Steve Pi"]
actions=["fight","marry","rescue","eat","join forces with","plant a garden with"]
places=["Themyscira","Hogwarts","Tatooine"," Wisconsin","Duluth", "the jungle","an oasis", "Faerie"]

hero=random.choice(names)
hero_job=random.choice(roles)

sidekick = random.choice(names)
villain_name = random.choice(names)
villain = random.choice(roles)
place1=random.choice(places)
action1=random.choice(actions)

name=input('What is your name?')
print("Hello, " + name)

Macguffin=input('What is the secret item? ')
print("Okay, we're looking for the "+Macguffin)


story1=("Once upon a time, there was a " + hero_job + " named " + hero+". They were on a quest to find a "+Macguffin+". While wandering, they found a castle.")

print(story1)

#if the answer is no, program prints story2.
story2="Our hero, " + hero+ " avoids the castle. It is dark and foreboding. Instead they run past and meet up with " +sidekick+". "+sidekick+" says they know where the "+Macguffin+" is and will tell you, for a fee." 
#if the answer is yes, program prints story3.
story3="Our hero, " + hero+ ", enters the castle. There, they find an evil "+ villain +". 'I am "+ villain_name+"! How dare you enter my castle!' Now I have to "+action1+" you!"

marker=0
path =0
while marker == 0:
    ending=input("Do they go into the castle? ")
    if ending=="no":
        print(story2)
        marker+=1
        path+=1
    elif ending=="yes":
        print(story3)
        marker+=1
        path+=2
    else:
        print("Please type yes or no.")


story4="You pay "+sidekick+". They tell you that the "+Macguffin+" is in "+place1+"."
story5="You do not pay "+sidekick+". They now want to "+action1+" you."


marker=0        
if path==1:
    while marker == 0:
        ending=input("Do you pay "+sidekick+"? ")
        if ending=="no":
            print(story5)
            marker+=1
            path==3
        elif ending=="yes":
            print(story4)
            marker+=1
            path==4
        else:
            print("Please type yes or no.")
            
story6=villain+" kills you. The end."
story7="You run away. You find the "+Macguffin+" by accident, and live happily ever after. The end."

if path==2:
    while marker == 0:
        ending=input("Do you run away? ")
        if ending=="no":
            print(story6)
            marker+=1
            path==5
        elif ending=="yes":
            print(story7)
            marker+=1
            path==6
        else:
            print("Please type yes or no.") 