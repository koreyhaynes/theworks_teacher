import random
secretNumber=random.randint(1,30)
print("I am thinking of a number between 1 and 30.")
print("You get five guesses.")
for guessesTaken in range(1,8):
    print("Take a guess.")
    guess=int(input())
    if guess<secretNumber:
        print("Too low.")
    elif guess>secretNumber:
        print("Too high.")
    else:
        break
if guess==secretNumber:
    print("Good guess!")
else:
    print("Sorry. The secret number was "+str(secretNumber))