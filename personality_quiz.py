#Setup from berryclip
import time
import random
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup (4, GPIO.OUT)
GPIO.setup (17, GPIO.OUT)
GPIO.setup (22, GPIO.OUT)
GPIO.setup (10, GPIO.OUT)
GPIO.setup (9, GPIO.OUT)
GPIO.setup (11, GPIO.OUT)
GPIO.setup (8, GPIO.OUT)
GPIO.setup (7, GPIO.IN)
GPIO.setup (25, GPIO.IN)

#We make a short script that takes input from the black and red buttons
#When the button is pressed, an answer is returned. We can use this many times
#7 is black, 25 is red
def ask():
  while GPIO.input(7)==True or GPIO.input(25)==True:
    pass
  while True:
    if GPIO.input(7)==True:
      return 'black'
    if GPIO.input(25)==True:
      return 'red'

#Create a class of object called choice. This will be how we store our answers
class choice(object):
    def __init__(self,name,score):
        self.name=name   #Our choice has a name
        self.score=score #Our choice also has a score
def byScore_key(choice):
    return choice.score #we want to be able to sort later by score, so we make a key
#Now we create four different possible choices, and set all their scores to zero
Kracken=choice('Kracken',0)
Phoenix=choice('Phoenix',0)
Dragon=choice('Dragon',0)
Unicorn_pegasus=choice('Unicorn_pegasus',0)

#Now we start the quiz!
print("lava or water?")
print("press the red button if lava, black if water.")
answer=ask()
if  answer=="black":
      Kracken.score+=1
      time.sleep(0.5)
if answer=="red":
      Dragon.score+=1
      time.sleep(0.5)

#We can also use text input instead of the buttons
print ("do you like birds,mammals,reptiles-amphibians,or fish better?")
answer=raw_input( "hit the A KEY for birds,B KEY for mammals,C KEY for reptiles-amphibians,and the D KEY for fish")
if answer=="A" or answer=='a':
    Phoenix.score+=1
elif answer=="B":
    Unicorn_pegasus.score+=1
elif answer=="C":
    Dragon.score+=1
elif answer=="D":
    Kracken.score+=1

#At the end, we combine all our choices into one list 
choices=[Kracken, Phoenix, Dragon, Unicorn_pegasus]
#Then we sort that list by the key Score that we made earlier
#We use reverse so that it gives the biggest number first
winners=sorted(choices, key=byScore_key, reverse=True)
#Now we take the first entry in the list (the zeroth one)
win=winners[0]
#And we print its name. This is the winner!
print(win.name)
