# README #

This is intended for the teacher's Pi in Code Camp. It has code snippets and examples that we do 
not want to give away to students or recreate from scratch every time! Enjoy these pieces of code. 



### Who do I talk to? ###

* Ask Korey (korey@theworks.org) or Sonia (sonia@theworks.org) for questions!